package cz.gopas.kalkulacka;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import cz.gopas.kalkulacka.databinding.FragmentCalcBinding;
import cz.gopas.kalkulacka.memory.MemoryEntity;
import cz.gopas.kalkulacka.memory.MemoryFragment;
import cz.gopas.kalkulacka.memory.MemoryViewModel;

public class CalcFragment extends MenuFragment {

    private static final String TAG = CalcFragment.class.getSimpleName();

    private FragmentCalcBinding binding;
    private CalcViewModel model;
    private MemoryViewModel memModel;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        model = new ViewModelProvider(this).get(CalcViewModel.class);
        memModel = new ViewModelProvider(requireActivity()).get(MemoryViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentCalcBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        model.result.observe(getViewLifecycleOwner(), liveDataResult -> {
            Log.d(TAG, "LiveData result");
            binding.res.setText(String.valueOf(liveDataResult));
            binding.share.setVisibility(View.VISIBLE);
            memModel.db.memoryDao().add(new MemoryEntity(liveDataResult));
        });

        memModel.selected.observe(getViewLifecycleOwner(), selectedMemory -> {
            if (selectedMemory != null) {
                binding.bText.getEditText().setText(String.valueOf(selectedMemory));
                memModel.selected.postValue(null);
            }
        });

        binding.calc.setOnClickListener(v -> {
            float a;
            try {
                a = Float.parseFloat(binding.aText.getEditText().getText().toString());
            } catch (Throwable t) {
                Log.w(TAG, t);
                binding.aText.setError(getString(R.string.error, t.getLocalizedMessage()));
                return;
            }
            binding.aText.setError(null);

            float b;
            try {
                b = Float.parseFloat(binding.bText.getEditText().getText().toString());
            } catch (Throwable t) {
                Log.w(TAG, t);
                binding.bText.setError(getString(R.string.error, t.getLocalizedMessage()));
                return;
            }
            binding.bText.setError(null);

            model.calc(a, b, binding.op.getCheckedRadioButtonId());
        });

        binding.share.setOnClickListener(v -> share());

        binding.ans.setOnClickListener(v -> binding.bText.getEditText().setText(String.valueOf(model.getAns())));

        binding.mem.setOnClickListener(v -> {
            getParentFragmentManager().beginTransaction().
                    replace(android.R.id.content, new MemoryFragment())
                    .addToBackStack(null)
                    .commit();
        });
    }

    /*
    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (!binding.res.getText().toString().isEmpty()) {
            binding.share.setVisibility(View.VISIBLE);
        }
    }
     */

    @Override
    public void onDestroyView() {
        binding = null;
        super.onDestroyView();
    }

    private void share() {
        Log.d(TAG, "Share");
        final Intent intent = new Intent(Intent.ACTION_SEND)
                .putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text, binding.res.getText()))
                .setType("text/plain");
        startActivity(intent);
    }
}
