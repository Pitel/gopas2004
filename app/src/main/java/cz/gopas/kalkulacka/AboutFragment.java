package cz.gopas.kalkulacka;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.textview.MaterialTextView;

public class AboutFragment extends Fragment {
    public AboutFragment() { // Required empty constructor!
        super(R.layout.fragment_about);
    }

    public AboutFragment(String author) {
        this();
        final Bundle args = new Bundle(1);
        args.putString("author", author);
        setArguments(args);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MaterialTextView) view).setText(getString(R.string.made_by, requireArguments().getString("author")));
        //view.setOnClickListener(v -> requireFragmentManager().popBackStack());
    }

    @Override
    public void onStart() {
        super.onStart();
        ((AppCompatActivity) requireActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
