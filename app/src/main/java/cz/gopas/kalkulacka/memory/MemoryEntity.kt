package cz.gopas.kalkulacka.memory

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "mems")
data class MemoryEntity @JvmOverloads constructor(
        val value: Float,
        @PrimaryKey(autoGenerate = true)
        val id: Long = 0
)