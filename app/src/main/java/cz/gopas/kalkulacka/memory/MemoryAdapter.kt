package cz.gopas.kalkulacka.memory

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer

class MemoryAdapter(val onClick: (Float) -> Unit) : ListAdapter<MemoryEntity, MemoryAdapter.MemoryViewHolder>(DIFF) {

    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int) = getItem(position).id

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MemoryViewHolder(
            LayoutInflater.from(parent.context).inflate(android.R.layout.simple_list_item_1, parent, false)
    )

    override fun onBindViewHolder(holder: MemoryViewHolder, position: Int) {
        holder.text.text = getItem(position).value.toString()
        holder.containerView.setOnClickListener { onClick(getItem(position).value) }
    }

    inner class MemoryViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        val text: TextView = containerView.findViewById(android.R.id.text1)
    }

    private companion object {
        private val DIFF = object : DiffUtil.ItemCallback<MemoryEntity>() {
            override fun areItemsTheSame(oldItem: MemoryEntity, newItem: MemoryEntity) = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: MemoryEntity, newItem: MemoryEntity) = oldItem == newItem
        }
    }
}