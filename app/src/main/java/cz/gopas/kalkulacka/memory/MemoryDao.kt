package cz.gopas.kalkulacka.memory

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface MemoryDao {
    @Query("SELECT * FROM mems ORDER BY id DESC")
    fun getAll(): LiveData<List<MemoryEntity>>

    @Insert
    fun add(entity: MemoryEntity)
}