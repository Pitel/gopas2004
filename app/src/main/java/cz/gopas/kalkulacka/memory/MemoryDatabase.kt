package cz.gopas.kalkulacka.memory

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [MemoryEntity::class], version = 1)
abstract class MemoryDatabase: RoomDatabase() {
    abstract fun memoryDao(): MemoryDao
}