package cz.gopas.kalkulacka.memory

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.RecyclerView
import androidx.lifecycle.observe
import cz.gopas.kalkulacka.MenuFragment
import cz.gopas.kalkulacka.R

class MemoryFragment : MenuFragment(R.layout.fragment_memory) {

    val memModel: MemoryViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val memAdapter = MemoryAdapter {
            memModel.selected.postValue(it)
            parentFragmentManager.popBackStack()
        }
        with(view as RecyclerView) {
            setHasFixedSize(true)
            adapter = memAdapter
        }
        memModel.db.memoryDao().getAll().observe(viewLifecycleOwner) {
            memAdapter.submitList(it)
        }
    }

    override fun onStart() {
        super.onStart()
        (requireActivity() as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}