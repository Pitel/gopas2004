package cz.gopas.kalkulacka.memory

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.room.Room

class MemoryViewModel(app: Application) : AndroidViewModel(app) {
    @JvmField
    val selected = MutableLiveData<Float?>()

    @JvmField
    val db = Room.databaseBuilder(app, MemoryDatabase::class.java, "memDbFile")
            .allowMainThreadQueries()
            .build()
}