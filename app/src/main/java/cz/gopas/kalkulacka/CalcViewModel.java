package cz.gopas.kalkulacka;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.preference.PreferenceManager;

public class CalcViewModel extends AndroidViewModel {
    private static final String TAG = CalcViewModel.class.getSimpleName();

    private final MutableLiveData<Float> _result = new MutableLiveData<>();
    public final LiveData<Float> result = _result;

    private SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplication());

    public CalcViewModel(@NonNull Application application) {
        super(application);
    }

    public void calc(float a, float b, @IdRes int op) {
        Log.d(TAG, "Calc!");

        new Thread(() -> {
            float x = Float.NaN;

            switch (op) {
                case R.id.add:
                    x = a + b;
                    break;
                case R.id.sub:
                    x = a - b;
                    break;
                case R.id.mul:
                    x = a * b;
                    break;
                case R.id.div:
                    x = a / b;
                    /*
                    if (b == 0) {
                        throw new NullPointerException("Zero division");
                    }
                     */
                    break;
            }

            prefs.edit().putFloat("prefs_result", x).apply();
            _result.postValue(x);
        }).start();
    }

    public float getAns() {
        return prefs.getFloat("prefs_result", 0);
    }
}
